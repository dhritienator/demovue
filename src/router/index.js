import { createRouter, createWebHistory } from 'vue-router'
import Home from '../views/Home.vue'
import SignupForm from '../components/SignupForm.vue'
import Product from '../views/Product.vue'
import Postorder from '../views/Postorder.vue'
import test from '../views/test.vue'

const routes = [
  {
    path: '/Home',
    name: 'Home',
    component: Home
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path: '/',
    name: 'SignupForm',
    component: SignupForm
  },
  {
    path: '/products',
    name: 'Product',
    component: Product
  },
  {
    path: '/PostOrder',
    name: 'Postorder',
    component: Postorder
  },
  {
    path: '/test',
    name: 'Test',
    component: test
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
